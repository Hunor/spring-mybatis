package com.example.springmybatis;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface ActorMapper {

    @Select("SELECT * FROM actor WHERE actor_id = #{actor_id}")
    Actor findById(@Param("actor_id") int actor_id);
}
