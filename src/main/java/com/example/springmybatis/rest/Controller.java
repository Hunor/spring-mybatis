package com.example.springmybatis.rest;

import com.example.springmybatis.Actor;
import com.example.springmybatis.ActorMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    private final ActorMapper actorMapper;

    public Controller(ActorMapper actorMapper) {
        this.actorMapper = actorMapper;
    }

    @GetMapping("/actors/{id}")
    public ResponseEntity<Actor> getActorById(@PathVariable int id){
        Actor result = this.actorMapper.findById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
